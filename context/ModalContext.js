import React, { createContext, useReducer } from 'react';

export const ModalContext = createContext();

const reducer = (state, action) => {
    if (typeof action !== 'object') return false;
    return { ...JSON.parse(JSON.stringify(state)), ...JSON.parse(JSON.stringify(action)) };
};
const initialState = {
    isOpen: false,
    product: {
        id: '',
        name: '',
        img:'',
        price: '',
        description: '',
    }
}
const ModalContextProvider = ({ children }) => {
    const [modalState, dispatchModalState] = useReducer(reducer, initialState);
    const contextData = {
        modalState,
        dispatchModalState
    };

    return <ModalContext.Provider value={contextData} >{children}</ModalContext.Provider >;
};

export default ModalContextProvider;