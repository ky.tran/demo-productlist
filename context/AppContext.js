import { createContext, useReducer } from "react";

export const AppContext = createContext()
const reducer = (state, action) => {
    if (typeof action !== "object") return false
    return { ...state, ...action }
}
const initialState = {
    products: [],
    isLoading: false,
    isError: false
}
const AppContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    return (
        <AppContext.Provider value={{state, dispatch}}>
            {children}
        </AppContext.Provider>
    )
}
export default AppContextProvider