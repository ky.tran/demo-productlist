import { Container, Grid } from '@mui/material'
import React, { useContext, useEffect, useRef, useState } from 'react'
import Head from 'next/head'
import CardProduct from '../../components/CardProduct'
import { useRouter } from 'next/router'
import { AppContext } from '../../context/AppContext'
import Search from '../../components/Search'

const Products = ({ products }) => {
    const { state,dispatch } = useContext(AppContext)
    const router = useRouter()
    useEffect(() => {
        if (!state.products.length) {
            dispatch({products})
        }
        
    }, [products])
    return (
        <>
            <Head>
                <title>Products</title>
            </Head>
            <Container maxWidth='xl'>
                <Search />
                <Grid container spacing={2}>
                    {products.map(product => (
                        <Grid key={product.id} item xl={3} md={4} sm={6} sx={{ display: 'flex', justifyContent: 'center' }} >
                            <CardProduct
                                id={product.id}
                                name={product.name}
                                category={product.category}
                                description={product.description}
                                price={product.price}
                            />
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </>
    )
}

export default Products

export const getServerSideProps = async (context) => {
    let query = '';
    const prefixUrl = context.req.headers.host
    if (Object.keys(context.query).length !== 0) {
        query = `search?search=${context.query.search}&category=${context.query.category}`
    }
     else {
        query = 'getProducts'
    }
    const res = await fetch(`https://${prefixUrl}/api/${query}`)
    const products = await res.json()
    return {
        props: { products }
    }
}