export default function handler(req, res) {
  const arr = [{
    id: '1',
    img: '/images/jordan-1.jpg',
    category: 'sport',
    name: "Jordan 5",
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    price: 340
  }, {
    id: '2',
    img: '/images/jordan-1.jpg',
    category: 'sport',
    name: "Jordan 1",
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    price: 532
  }, {
    id: '3',
    img: '/images/jordan-1.jpg',
    category: 'sneacky',
    name: "Jordan 2",
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    price: 390
  }, {
    id: '4',
    img: '/images/jordan-1.jpg',
    category: 'sneacky',
    name: "Jordan 3",
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    price: 497
  }, {
    id: '5',
    img: '/images/jordan-1.jpg',
    category: 'sneacky',
    name: "Jordan 4",
    description: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    price: 589
  }]

  if (Object.keys(req.query).length > 0) {
    const results = arr.filter((product) => {
      if (req.query.category === 'all') {
        return product.name.toLowerCase().includes(req.query?.search.toLowerCase())
      }
      return (req.query.category === product.category) && product.name.toLowerCase().includes(req.query?.search.toLowerCase())
    })
    res.status(200).json(results)
  } else {
    res.status(200).json(arr)
  }
}