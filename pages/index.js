import {  Grid, Typography } from "@mui/material";
import { useContext, useEffect } from "react";
import { AppContext } from "../context/AppContext";

export default function Home({ products }) {
  const { dispatch } = useContext(AppContext)
  useEffect(() => {
    dispatch({products})
  }, [dispatch,products])
  return (
    <Grid container sx={{justifyContent:'center'}}>
      <Grid item>
          <Typography variant="h3">Welcome to Nickey</Typography>
      </Grid>
    </Grid>
  )
}

export const getServerSideProps = async () => {
  const prefixUrl = context.req.headers.host

  const res = await fetch(`https://${prefixUrl}/api/getProducts`)
  const products = await res.json()
  
  return {
    props: { products }
  }
}
