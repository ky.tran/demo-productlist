import { Box, Button, Card, CardActions, CardContent, Grid, Typography } from '@mui/material'
import { makeStyles } from '@mui/styles'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useStylesButton } from './common/button'

const useStyles = makeStyles({
    card: {
        width: 320,
        height: 430,
        '&:hover': {
            width: 330,
            opacity: 0.8,
            cursor: 'pointer'
        }
    },
})

const CardProduct = ({ id, name, category ='', description, price }) => {
    const btnClasses = useStylesButton()
    const classes = useStyles()
    const router = useRouter()
    const onClick = () => {
        router.push({
            pathname: `products/${id}`,
        })
    }
    return (
        <>
            <Card className={classes.card}>
                <Box sx={{ padding: 0 }} onClick={onClick}>
                    <Image
                        layout="responsive"
                        objectFit="cover"
                        width={6}
                        height={4}
                        src="/images/jordan-1.jpg"
                        alt="jordan"
                    />
                    <CardContent>
                        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            <Typography variant="h6" component="div">
                                {name}
                            </Typography>
                            <Typography sx={{ color: 'red' }} variant="h6" component="div">
                                {price} $
                            </Typography>
                        </Box>
                        <Typography sx={{paddingBottom: 1,fontWeight:400,fontSize:18}} variant="body" component="div">
                            Type: {category?.toUpperCase()}
                        </Typography>
                        <Typography sx={{
                            display: '-webkit-box',
                            overflow: 'hidden',
                            WebkitBoxOrient: 'vertical',
                            WebkitLineClamp: 3,
                        }} variant="body2" color="text.secondary">
                            {description}
                        </Typography>
                    </CardContent>
                </Box>
                <CardActions sx={{ justifyContent: 'flex-end', paddingRight: 2 }} >
                    <Button className={btnClasses.btn} size="small">Buy now</Button>
                    <Button className={btnClasses.btn} size="small">Add to cart</Button>
                </CardActions>
            </Card>
        </>
    )
}

export default CardProduct