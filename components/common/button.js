import { makeStyles } from '@mui/styles'

export const useStylesButton = makeStyles({
    btn: {
        '&:hover': {
            background: "#bbbbbb"
        },
        background: '#fff',
        color: 'black',
        outline: 'black',
        border: '1px solid',
        boxSizing: 'border-box'
    },
    btnCancel: {
        background: '#E74646',
        color: 'black',
        border: '1px solid ',
        boxSizing: 'border-box',
        '&:hover': {
            background: '#f56b6b',
        },
    },
    btnSubmit: {
        background: '#46C66A',
        color: 'black',
        border: '1px solid ',
        boxSizing: 'border-box',
        '&:hover': {
            background: '#83e09d',
        },
    },
    link: {
        '&:hover': {
            opacity: 0.7
        }
    }
})